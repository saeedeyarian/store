import React, { useContext } from 'react';
import {Link} from "react-router-dom";
import { isInCart, quantityCount, shorten } from '../shorten/functions';
import { cartContext } from '../../context/CartContextProvider';
import trash from '../icons/trash.svg'

import styles from "../scss/product.module.css";



const Product = ({productData}) => {

    const {state, dispatch} = useContext(cartContext);

    return (
        <div className={styles.container}>
            <img className={styles.productImage} src={productData.image} alt="product" />
                <h3>{shorten(productData.title)}</h3>
                <p>{productData.price}</p>
                <div className={styles.details}>
                    <Link to={`/products/${productData.id}`}>Details</Link>
                    <div className={styles.buttonsContainer}>
                        {
                            isInCart(state, productData.id) ?
                            <button className={styles.smallButton} onClick={() => dispatch({type:"INCREASE", payload: productData})}>+</button> :
                            <button onClick={() => dispatch({type:"ADD_ITEM", payload: productData})}> Add to Cart </button>
                        }
                        {quantityCount (state, productData.id) > 0 &&<span>{quantityCount (state, productData.id)}</span>}
                        {
                            quantityCount (state, productData.id) >1 && 
                            <button className={styles.smallButton} onClick={() => dispatch({type: "DECREASE", payload:productData})}> - </button>
                        }
                        {
                            quantityCount (state, productData.id) === 1 &&
                            <button className={styles.smallButton} onClick={() => dispatch({type: "REMOVE_ITEM" , payload:productData})}>
                                <img src={trash} alt='trash'/>
                            </button>
                        }
                    </div>
                </div>
        </div>
    );
};

export default Product;