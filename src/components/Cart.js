import React, { useContext } from 'react';
import { shorten } from './shorten/functions';
import { cartContext } from '../context/CartContextProvider';
import trash from './icons/trash.svg';

import styles from "./scss/cart.module.css";

const Cart = (props) => {

    const {dispatch} = useContext(cartContext);
    const {image, price, title, quantity} = props.data

    return (
        <div className={styles.cartContainer}>
            <img className={styles.productImage} src={image} alt="product"/>
            <div className={styles.data}>
                <h3>{shorten(title)}</h3>
                <p>{price} $</p>
            </div>
            <div>
                <span className={styles.quantitiy}>{quantity}</span>
            </div>
            <div className={styles.buttonContainer}>
                {quantity > 1 ?
                    <button onClick={() => dispatch ({type:"DECREASE", payload: props.data})}>-</button> :
                    <button onClick={() => dispatch ({type:"REMOVE_ITEM", payload:props.data})}><img src={trash} alt="trash" style={{width: "20px"}}/></button>
                }
                <button onClick={() => dispatch ({type: "INCREASE", payload:props.data})}>+</button>
            </div>
        </div>
    );
};

export default Cart;