import React, { useReducer } from 'react';

const initialState = {
    selectedItems : [],
    itemCounter : 0,
    total : 0,
    checkout: false
}

const cartReducer = (state,action) => {
    switch (action.type) {
        case "ADD-ITEM" :
            if (!state.selectedItems.find( item => item.id === action.payload.id)) {
                state.selectedItems.push ({
                    ...action.payload,
                    quantity :1
                })
            }
            return {
                ...state,
                selectedItems : [...selectedItems]
            }
        case "REMOVE-ITEM" :
            const newSelectedItem = state.selectedItems.filter (
                item.id !== action.payload.id
            )
            return {
                ...state,
                selectedItems : [...newSelectedItem]
            }
        case "INCREASE" :
            const indexI = state.selectedItems.findIndex (
                item => item.id === action.payload.id,
                state.selectedItems[indexI].quantity++
            )
            return { ...state }
        case "DECREASE" :
            const indexD = state.selectedItems.findIndex (
                item => item.id === action.payload.id,
                state.selectedItems[indexD].quantity--
            )
            return { ...state }
        case "CHECKOUT" :
            return {
                selectedItems : [],
                itemCounter : 0,
                total : 0,
                checkout: true
            }
        case "CLEAR" :
            return {
                selectedItems : [],
                itemCounter : 0,
                total : 0,
                checkout: false
            } 
    }
}

const CartContext = () => {

    const { state, dispatch}  = useReducer(cartReducer, initialState);

    return (
        <div>
            
        </div>
    );
};

export default CartContext;