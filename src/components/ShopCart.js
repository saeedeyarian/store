import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { cartContext } from '../context/CartContextProvider';
import Cart from './Cart'

import styles from "./scss/shopCart.module.css";

const ShopCart = () => {

    const {state, dispatch} = useContext(cartContext)

    return (
        <div className={styles.contain}>
            <div className={styles.cartContain}>
                {
                    state.selectedItems.map (item => <Cart key={item.id} data={item}/>)
                }
            </div>
            {
                state.itemCounter > 0 &&
                <div className={styles.payments}>
                    <p><span>total counter : </span>{state.itemCounter}</p>
                    <p><span>total payment : </span> {state.total}</p>
                    <div className={styles.buttonContainer}>
                        <button className={styles.clear} onClick={() => dispatch ({type: "CLEAR"})}>clear</button>
                        <button className={styles.checkout} onClick={() => dispatch ({type: "CHECKOUT"})}>checkout</button>
                    </div>
                </div>
            }
            {
                state.checkout && <div  className={styles.complete}>
                    <h3>checkout successfully</h3>
                    <Link to ="/products"> buy more</Link>
                </div>
            }
            {
                !state.checkout && state.itemCounter === 0 &&
                <div className={styles.complete}>
                    <h3>Wants to Buy?</h3>
                    <Link to ="/products"> Go to shop</Link>
                </div>
            }
        </div>
    );
};

export default ShopCart;