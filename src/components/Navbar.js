import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { cartContext } from '../context/CartContextProvider';
import shop from './icons/shop.svg';
import styles from './scss/navbar.module.css';

const Navbar = () => {

    const {state} = useContext(cartContext)

    return (
        <div className={styles.mainContainer}>
            <div className={styles.navbar}>
                <Link to="/products" className={styles.productLink}>Products</Link>
                <div className={styles.shopIcon}>
                    <Link to="/ShopCart">
                        <img src={shop} alt="shop"/>
                    </Link>
                        <span>{state.itemCounter}</span>
                </div>
            </div>
        </div>
    );
};

export default Navbar;