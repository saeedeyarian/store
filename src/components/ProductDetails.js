import React,{useContext} from 'react';
import { Link, useParams } from 'react-router-dom';
import { productsContext } from '../context/ProdoctContextProvider';
import styles from './scss/productDetails.module.css';

const ProductDetails = () => {

    const data = useContext (productsContext);
    const params = useParams();
    const id = params.id;
    const product = data[id-1]
    const {image,title,price,category,description} = product;

    return (
        <div className={styles.container}>
            <img src={image} alt="product"/>
            <div className={styles.textContainer}>
                <h3 className={styles.title}>{title}</h3>
                <p className={styles.description}>{description}</p>
                <p className={styles.category}><span>category:</span> {category}</p>
                <div className={styles.buttonContainer}>
                    <p className={styles.price}>{price} $</p>
                    <Link to="/products">BACK TO SHOP</Link>
                </div>
            </div>
        </div>
    );
};

export default ProductDetails;