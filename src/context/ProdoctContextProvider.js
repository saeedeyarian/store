import React, {useState,useEffect,createContext} from 'react';
import { getProdocts } from '../services/Api';

export const productsContext = createContext()

const ProdoctContextProvider = ({children}) => {

    const [products,setProducts] = useState([])

    useEffect ( () => {

        const fetchApi = async() => {
            setProducts (await getProdocts())
        }

        fetchApi()

    },[])

    return (
        <productsContext.Provider value={products}>
            {children}
        </productsContext.Provider>
    );
};

export default ProdoctContextProvider;