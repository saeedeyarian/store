import './App.css';
import { Routes, Route, Navigate} from 'react-router-dom';
import ProdoctContextProvider from "./context/ProdoctContextProvider";
import Store from "./components/Store";
import ShopCart from './components/ShopCart';
import ProductDetails from './components/ProductDetails';
import CartContextProvider from './context/CartContextProvider';
import Navbar from './components/Navbar';

function App() {
  return (
    <ProdoctContextProvider>
    <CartContextProvider>
      <Navbar/>
      <Routes>
        <Route path='/products' element={<Store/>} />
        <Route path='/products/:id' element={<ProductDetails/>} />
        <Route path='/ShopCart' element={<ShopCart/>}/>
        <Route path="/*" element={<Navigate to="products"/>} />
      </Routes>
    </CartContextProvider>
    </ProdoctContextProvider>
  );
}

export default App;
